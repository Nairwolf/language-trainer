#!/usr/bin/env python3

import sys, os
import random
import os.path
from argparse import ArgumentParser

def get_words(line):
    """
        Take a line and returns a list of words
        line: * Buenos dias ! : Bonjour
        
        Returns `word_list` which is a list with the two versions of the same word
        word_list = ['Buenos dias !', 'Bonjour']
    """
    word_list = []
    if line.startswith('*'):
        line = line.replace('*', '')
        for sentence in line.split(':'):
            word = sentence.strip()
            word_list.append(word)
    return word_list

def pick_a_word(word, all_option=False, fr_es=False, es_fr=False):
    """
        Based on options, create target and solution variables.
        word is ['Buenos dias !', 'Bonjour']

        Returns the tuple (target, solution)
    """
    wtype = random.randint(0, 1)

    es_word = word[0]
    fr_word = word[1]
    
    if fr_es:
        target = fr_word
        solution = es_word
    elif es_fr:
        target = es_word
        solution = fr_word
    elif wtype == 1:
        target = es_word
        solution = fr_word
    else:
        target = fr_word
        solution = es_word

    return target, solution

def create_line_word(target, solution):
    #target = target.replace('(', ': ').replace(')', '')
    wordline = "* "+target+" : "+solution

    return wordline

def write_errors_file(failed_words, filename):
    failed_words = set(failed_words)
    with open('errors_'+filename, 'a') as f:
        for wordline in failed_words:
            f.write(wordline+'\n')

def _parse_args():
    parser = ArgumentParser(description='Practice and learn foreign words.')
    parser.add_argument("file",
            help="File used to learn words")

    parser.add_argument('-a', '--all', 
            help="Take all words from the list",
            action="store_true"
            )

    parser.add_argument('--fr-es',
            help="Translate words from French to Spanish",
            action="store_true"
            )

    parser.add_argument('--es-fr',
            help="Translate words from Spanish to French",
            action="store_true"
            )

    return parser.parse_args()

if __name__ == '__main__':
    args = _parse_args()
    filepath = args.file
    filename = os.path.basename(filepath)

    if not os.path.exists(filepath):
        print("Enter an existing filename")
        exit()

    words = [] # list of words to translate 
    failed_words = [] # list of failed words

    # Construct the list of words to translate
    with open(filepath, 'r') as f:
        lines = f.read().splitlines()
        for line in lines:
            word_list = get_words(line)
            if word_list:
                words.append(word_list)

    # Shuffle the list of words to translate
    random.shuffle(words)

    # Retrieves the number of iterations of the exercise
    while True:
        if args.all:
            nb = len(words)
        else:
            nb = input("How many times do you want to play ?\n")
        try:
            nb = int(nb)
        except ValueError:
            print("Enter a positive value !")
            continue
        if nb >= 0:
            break
        else:
            print("Enter a positive value !")
            continue
        
    nb_success = 0

    for i in range(0, int(nb)):
        print("****************************")
        print("Exercice n°{}".format(i+1))

        if args.all:
            target, solution = pick_a_word(words[i], fr_es=args.fr_es, es_fr=args.es_fr)
        else:
            target, solution = pick_a_word(random.choice(words), fr_es=args.fr_es, es_fr=args.es_fr)

        answer = input('What does this word mean "{}" ?\n'.format(target))

        if answer == solution:
            print("Correct !")
            nb_success += 1
        else:
            print('The solution is "{}"'.format(solution))
            while True:
                rst = input("Is it correct ? (y/n) ")
                if rst in ('y', 'n'):
                    if rst == 'y':
                        nb_success += 1
                    else:
                        failed_word = create_line_word(target, solution)
                        failed_words.append(failed_word)
                    break
                else:
                    print("'{}' isn't a correct answer".format(rst))
                    continue

        percentage = round((nb_success / (i+1))*100)
        print("****************************")
        print("Good answers : {}/{}".format(nb_success, nb))
        print("Success rate : {}%".format(percentage))
        
    if not filename.startswith('errors'):
        write_errors_file(failed_words, filename)
