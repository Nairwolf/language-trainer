#!/usr/bin/env python3

import sys

def sanitize_line(line, exception):
    if not line.startswith(exception):
        line = '* ' + line
    return line

if __name__ == '__main__':
    try:
        FILE = sys.argv[1]
    except IndexError:
        print("Usage: reformat.py FILEPATH")
        exit()
    exception = ('*', '#', '\n')
    with open(FILE, 'r') as fr:
        content = ''
        for line in fr:
            line = sanitize_line(line, exception)
            content += line

    with open(FILE, 'w') as fw:
        fw.write(content)
